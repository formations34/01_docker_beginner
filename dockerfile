# image de base
FROM node:19-alpine3.16

#Metadonnées
LABEL author="BLEU Koty" desc="Un site web basic"

ENV PORT=4200
#Spécification du répertoire applicatif
WORKDIR /yamb/web/mon-site

#Port exposé
EXPOSE ${PORT}

RUN npm install -g serve
RUN echo $PORT

# Opération de copie depuis le répertoire de la machine hôte vers l'image docker
COPY index.html ./

# serve -s . -l 4200
ENTRYPOINT [ "sh" ]
CMD [ "-c" ,"serve -s . -l ${PORT}" ]